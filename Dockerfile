FROM node:8.11.2

RUN mkdir /app

WORKDIR /app

COPY . /app

RUN apt-get update -y || true && apt-get install -y vim curl git

RUN npm i -S hexo-generator-search hexo-generator-feed hexo-renderer-less hexo-autoprefixer hexo-generator-json-content hexo-fontawesome
RUN npm install hexo-cli@3.1.0 -g
RUN npm install
