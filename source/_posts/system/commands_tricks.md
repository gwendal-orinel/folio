---
title: Commands tips & tricks
date: 2020-02-20  12:00:00
tags:
 - show ip
 - hostname
 - shell
 - proxy
 - tips & tricks
categories: 
 - Système
---

#### Infos Reseau

```
IP=$(ip -4 -o a | sed -e '/^2:/!d;s/^.*inet //;s/\/.*$//g')
HOSTNAME=$(hostname -s)
```

#### Modification des hosts et dns

```
[[ $(grep -E "$IP(.*)$(hostname -s)(.*)" /etc/hosts) ]] || echo -e "$IP    $(hostname -s)" >> /etc/hosts
```

#### Verification execution en root

```
[[ $(whoami) != root ]] && { echo "ERREUR : Veuillez exécuter ce script en tant que root avec la commande sudo !"; exit 1; }
```

#### Proxy

APT:

```
echo -e "Acquire::http::Proxy \"${PROXY_URL}/\";" > /etc/apt/apt.conf.d/proxy.conf
```

System:
```
PROXY_URL="http://USER:PASS@HOST:PORT"
echo -e "export http_proxy='${PROXY_URL}'\n
	     export https_proxy='${PROXY_URL}'\n
	     export no_proxy=EXCLUDED_DOMAIN.fr,localhost,127.0.0.1,${HOSTNAME}" > /etc/profile.d/proxy.sh
source /etc/profile.d/proxy.sh
```

Maven:

```
mkdir -p ~/.m2
echo ${http_proxy} | tr "/:@" " " | awk '{print "<settings>\n<proxies>\n<proxy>\n<id>proxy</id>\n<active>true</active>\n<protocol>"$1"</protocol>\n<host>"$4"</host>\n<port>"$5"</port>\n<username>"$2"</username>\n<password>"$3"</password>\n</proxy>\n</proxies>\n</settings>"}' > ~/.m2/settings.xml
```

Curl:

```
echo "proxy = ${http_proxy}" > ~/.curlrc
```


#### Decompresser à la volée

```
curl -s $PACKAGE_REGISTRY/stedolan/jq/1.6/jq-1.6.tar.gz | tar xz -C /usr/bin/ && chmod +x /usr/bin/jq

```

#### Add Iptables

```
iptables -A INPUT -p tcp -m tcp --dport 443 -j ACCEPT && iptables -A INPUT -p tcp -m tcp --dport 80 -j ACCEPT && iptables-save 2> /dev/null > /etc/sysconfig/iptables
```

#### Optimisation memoire Java (java heap size)

```
-Xmx2048m -Xms512m -XX:MaxPermSize=2048m -XX:+UseConcMarkSweepGC -XX:ReservedCodeCacheSize=128m -XX:+HeapDumpOnOutOfMemoryError
```

#### Changement de cible binaire

```
update-alternatives --set php /usr/bin/php5.6

Alias alternative from vars:
echo "[[ \${php}  ]] && alias php='update-alternatives --set php /usr/bin/php\${php} > /dev/null; php' \
	\n[[ \${node} ]] && alias node='nvm use \${node} >/dev/null; node'" >> ~/.bashrc
```

#### Manipulation de variables

```
Variable avec nom dynamique (shell): 
varname="toto"
varcontent="content"
$varname=$varcontent

name=${varname}
data=${!varname}

Variable avec nom dynamique (Groovy): 
def OC_CONFIG_PATH="OC_CONFIG_AA"
evaluate("env.${OC_CONFIG_PATH} = \"--config='/tmp/$OC_CONFIG_PATH/config'\" ")
env.OC_CONFIG_PATH="$OC_CONFIG_PATH"
OC_CONFIG_PATH=\${!OC_CONFIG_PATH}
env.KUBECONFIG="/tmp/AA/oc_config_${JOB_BASE_NAME}"
```

#### Sed tricks

```
change maven version
sed -i -r -e 's#<source>(.*)</source>#<source>1.8</source>#g' -e 's#<target>(.*)</target>#<target>1.8</target>#g' pom.xml

# Export 'exist*' groovy vars outside this shell
echo "\$(env | grep -E '_exist|_version' |sed 's/^/env./')" | tee ${WORKSPACE}/vars.groovy


```

#### Find tricks

```
find ${builder.buildDir}/ -name "*.sh" -exec chmod +x {} \\;
find ${builder.buildDir}/ -name "*.sh" -exec chmod 755 {} \\;

```


#### Import propre sql

```
mysql -u root -e "source /db.sql;"
```

#### Windows Startup applications

```
%AppData%\Microsoft\Windows\Start Menu\Programs\Startup
```

#### Systemd Authorize execution from alternative PATH

```
semanage fcontext -a -t bin_t "/data/pgsql/bin(/.*)?"
restorecon -RF /data/pgsql/bin
```

#### Tester la presence d'un executable:

```
command -v <BIN>

```

#### Pousser une commande en Background et la récupérer:

```
tail -f /dev/null 	#exemple
ctrl + z 			#stop le process
bg 					# le pousse en Background
fg 					# pour récupere le Process

```
