---
title: Docker tips & tricks
date: 2019-06-27  15:00:00
tags:
 - docker
 - tips & tricks
categories:
 - Système
---

#### Install latest docker & docker-compose

```
curl -sSL https://get.docker.com/ | sh
curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

#### Configure proxy for docker

```
mkdir -p /etc/systemd/system/docker.service.d
PROXY_URL='http:/USER:PASS@PROXY:PORT'
echo -e "[Service] \nEnvironment=\"HTTPS_PROXY=${PROXY_URL}\"\nEnvironment=\"NO_PROXY=localhost,127.0.0.1,${HOSTNAME}\"" > /etc/systemd/system/docker.service.d/http-proxy.conf
systemctl daemon-reload && systemctl restart docker
```

#### Change data directory & activate logrotate

```
mkdir -p /etc/docker/ /data
echo -e "{\"data-root\": \"/data/docker\",\"log-driver\": \"json-file\", \"log-opts\": { \"max-size\": \"100m\",\"max-file\": \"5\" }}}" | tee /etc/docker/daemon.json
systemctl daemon-reload && systemctl enable docker && systemctl restart docker
```

#### Clean by filter orphan images or regex container

```
docker rmi $(docker images -q -f 'dangling=true') 2> /dev/null || true
docker rm -f $(docker ps -aq -f 'name=NAME*') 2> /dev/null || true
```


#### Import many images from directory, apply tag by date and latest, and push to registry

```
Livraisons="2018-10-10 2019-01-21 2019-03-21 2019-03-26 2019-04-10 2019-04-11"

for livraison in $Livraisons;do
	for image in $(ls $IMAGES_PATH/$livraison/*.tar); do 
		name=$(docker load -q -i $image | awk '{print $3}');
		newtag=$(basename $name | sed -e "s/:.*$/:$(basename $(dirname $image))/g")
		latesttag=$(basename $name | sed -e 's/:.*$/:latest/g')
		docker tag $name $DOCKER_REGISTRY/$newtag; 
		docker tag $name $DOCKER_REGISTRY/$latesttag
		docker push $DOCKER_REGISTRY/$newtag 
		docker push $DOCKER_REGISTRY/$latesttag
		docker rmi $name >/dev/null; 
		echo -e "\n==> image $DOCKER_REGISTRY/$newtag imported\n"; 
	done
done
```

#### Dockerfile exemple multi-tool container

```
### Input variables ##
ARG http_proxy
ARG https_proxy
ARG no_proxy 

## put arg as env for futur use
ENV http_proxy=$http_proxy
ENV https_proxy=$https_proxy
ENV no_proxy=$no_proxy

# System Env
ENV TERM=xterm
ENV LC_ALL=C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive

# Package Env
ENV INSTALL="apt-utils software-properties-common vim dos2unix sqlite3 mysql-server mysql-client libpcre3-dev apache2 php-pear php-pecl-http maven"
ENV INSTALL_PHP="5.5 5.6 7.0 7.2"
ENV INSTALL_PHP_MODULES="xml mysql http mcrypt sqlite3 mbstring curl cli gd dev intl xsl memcache memcached zip apc"
ENV INSTALL_JAVA="openjdk-8-jdk openjdk-8-jre"
ENV INSTALL_NODEJS="8 9 10 11 12"

# Download others packages
RUN apt-get update -y && apt-get install -y software-properties-common apt-transport-https lsb-release

# Download the signing key for php repo
RUN wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg

# Add repo for php packages
RUN echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list && apt-get update -y

# Add nvm for nodejs managing
RUN wget -O install.sh  https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh; bash install.sh

# Installing packages
# System packages
RUN /bin/bash -c 'for packages in ${INSTALL[@]};do apt-get install -y ${packages} ;done'

# Php & php modules packages
RUN /bin/bash -c 'for php_version in ${INSTALL_PHP[@]};do apt-get install -y php${php_version} ; for php_modules in ${INSTALL_PHP_MODULES[@]};do apt-get install -y php${php_version}-${php_modules} ;done ;done'

# Java packages
RUN /bin/bash -c 'for java_version in ${INSTALL_JAVA[@]};do apt-get install -y ${java_version} ;done'

# Nodejs packages
RUN /bin/bash -c 'source ~/.profile 2>/dev/null; for node_version in ${INSTALL_NODEJS[@]};do nvm install ${node_version} ;done'


```

