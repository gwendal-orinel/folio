---
title: GIT- Appliquer des changements d'une branche autonome à une autre
date: 2019-12-19  15:00:00
tags:
 - Git
 - Branch
categories: 
 - Système
---

## Pour appliquer les changement d’une branche à une autre :

#### Comparer la liste des fichiers de master par rapport à une autre branche (demo) avec exclusion de fichier

```
git diff master..demo --stat ':(exclude)fichier1' ':(exclude)README.md'
```

#### Comparer le contenu des fichiers de master par rapport à demo

```
git diff master..demo
```

#### Appliquer les changement de master sur metro pour le fichier fichier_2.txt

```
git checkout demo
git checkout --patch master fichier_2.txt
```

-> Options:
y: accepter les changements
n: refuser les changements
a: accepter tous les changements
e: editer manuellement le fichier (si la ligne à garder commence par - il faut juste le remplacer par un espace et supprimer la ligne correspondante qui commence par un +)

#### Cherrypick entre plusieurs repos

- Preparation
```
git clone https://repoA.git repoA
git clone https://repoB.git repoB
git clone https://repoC.git repoC

cd repoB/
git remote add repoA ../repoA
git remote add repoC ../repoC

cd ..

cd repoC/
git remote add repoA ../repoA
git remote add repoB ../repoB

```
- Utilisation
```
Recupération de un ou plusieurs commits de repoA vers repoB

cd repoB/
git fetch repoA

-> pour un seul commit:
git cherry-pick <commit_id>

-> pour un groupe de commits:
git cherry-pick <first_commit>..<last_commit>

git push
```


#### Application à grande échelle

```
for br in $(git branch --list | tr -d '*' | grep -v <SOURCE BRANCH>); do git checkout $br; git checkout --patch <SOURCE BRANCH> <FICHIER> ; done 
```

#### Authentification

```
Personal/project/group access tokens

curl "https://gitlab.example.com/api/v4/projects?private_token=<your_access_token>"

curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects"

OAuth-compliant headers:

curl --header "Authorization: Bearer <your_access_token>" "https://gitlab.example.com/api/v4/projects"

curl "https://oauth2:${CI_JOB_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git"
```

#### Pull and push back in Gitlab-ci

```
update_tree_on_git:
  stage: update_tree_on_git
  tags:
    - <target runner>
  variables:
    # Save Git token in CI/CD variables as GIT_PUSH_TOKEN
    CI_JOB_TOKEN: $GIT_PUSH_TOKEN
  before_script:
    # Set Git info
    - git config user.email "${GITLAB_USER_EMAIL}"
    - git config user.name "${GITLAB_USER_NAME}"
    - git remote add origin_push https://oauth2:${CI_JOB_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git
  script:
    - **make some stuff**
  after_script:
    # Save changes
    - git add <files> && git commit -m "<commit message>"
    - git push origin_push HEAD:${CI_COMMIT_BRANCH} -o ci.skip # Prevent triggering pipeline again
```


#### Save git credential
```
git config --global credential.helper store
```
