---
title: Entreprises où j'ai travaillé
date: 2019-06-01  10:00:00
categories: About
index: false
toc: false
tags: 
 - Experiences professionelles
---
<ul class="list-unstyled">
{% for item in  site.data.entreprises.list %}{% set entreprise = site.data.entreprises[item] %}
<li class="media">
  <div class="media-left mt-4">
  <img class="" src="/assets/images/{{ item }}.png" alt="">
  </div>
  <div class="media-body ml-3 mr -3">
    <h3>{{ entreprise.name }}</h3>
    {% fa_inline briefcase fas %} <cite>{{ entreprise.role }}</cite></br>
    {% fa_inline clock fas %} <cite>{{ entreprise.date }}</cite></br>
    {{ entreprise.desc }}
  </div>
  <div class="media-right mt-3">
    <a href="{{ entreprise.map }}"><button type="button" class="btn btn-primary btn-sm">{% fa_inline map-marker-alt fas %} </button></a>
    <a href="{{ entreprise.url }}"><button type="button" class="btn btn-secondary btn-sm">{% fa_inline globe fas %}</button></a>
  </div>
</li>
</br>

{% endfor %}

</ul>

