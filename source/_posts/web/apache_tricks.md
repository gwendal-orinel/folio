---
title: Apache2 tips & tricks
date: 2024-04-08  09:00:00
tags:
 - apache2
 - proxy
 - web
 - tips & tricks
categories: 
 - Web
---

#### Proxyfier un site et subpath différent

```
<VirtualHost *:8080>
  ServerName DOMAIN.FR

  SSLEngine on
  SSLCertificateFile    fullchain1.pem
  SSLCertificateKeyFile privkey1.pem

  SSLProxyEngine on
  SSLProxyVerify none
  SSLProxyCheckPeerCN off
  SSLProxyCheckPeerName off
  SSLProxyCheckPeerExpire off
  ProxyPreserveHost off
  ProxyRequests on

  ## Root path
  ProxyPass / https://<https website>/
  ProxyPassReverse / https://<https website>/
  

  ## Root subpath with websocket application
  ProxyPass /subpath/api/websocket ws://localhost:5800/api/websocket
  ProxyPassReverse /subpath/api/websocket ws://localhost:5800/api/websocket
  ProxyPass /subpath/ http://localhost:5800/
  ProxyPassReverse /subpath/ http://localhost:5800/
  RewriteEngine on
  RewriteCond %{HTTP:Upgrade} =websocket [NC]
  RewriteRule /subpath/(.*)  ws://localhost:5800/$1 [P,L]
  RewriteCond %{HTTP:Upgrade} !=websocket [NC]
  RewriteRule /subpath/(.*)  http://localhost:5800/$1 [P,L]
  
</VirtualHost>
```

#### Proxification derière un proxy existant (Apache < 5.2)

```
<VirtualHost *:443>

  ServerName DOMAIN

  SSLEngine on
  SSLProtocol -ALL +TLSv1.2
  # New cipher suite optionnal
  SSLCipherSuite ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256
  SSLHonorCipherOrder on
  SSLCertificateFile "CER.pem"
  SSLCertificateKeyFile "KEY.pem"
  SSLCACertificateFile "ca_SSL_chain.pem"

  ProxyPass / http://localhost:8081/ nocanon
  ProxyPassReverse / http://localhost:8081/
  RequestHeader set X-Forwarded-Proto "https"
  AllowEncodedSlashes NoDecode
  ProxyPass /subpath/ https://<https website>/
  ProxyPassReverse /subpath/ https://<https website>/

  SSLProxyEngine on
  SSLProxyVerify none
  SSLProxyCheckPeerCN off
  SSLProxyCheckPeerName off
  SSLProxyCheckPeerExpire off
  ProxyRemote https://<https website>/ 'http://<proxy_url>:<proxy port>'
  ProxyPreserveHost Off
  ProxyRequests Off

  RewriteEngine On
  RewriteCond %{HTTP:Upgrade} !=websocket [NC]
  RewriteRule /subpath/(.*)  https://<https website>/$1 [P,L]

  <Proxy https://<https website>/>
    Order deny,allow
    Allow from all
    SetEnv Proxy-Chain-Auth On
    RequestHeader set Proxy-Authorization "Basic <base64 encoded usrname:password>"
    #SetEnv proxy-initial-not-pooled 1
  </Proxy>

  ErrorLog logs/error_log
  CustomLog logs/access_log common

</VirtualHost>

```
