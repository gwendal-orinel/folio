---
title: Pipeline Devops pour Let's Encrypt
date: 2019-06-06  10:00:00
tags:
 - Certbot
 - Https
 - Let's Encrypt
 - Gitlab-ci
categories: 
 - Web
---
## Montage d'une pipeline DevOps pour Let's Encrypt

Afin de faciliter mes futurs projecs et de pouvoir les sécuriser, j'ai décider d'utiliser Let's Encrypt de façon automatique.

Il était déja possible d'utiliser Let's Encrypt pour certifier un nom de domain ou sous domaine, aujourd'hui il est possible de certifier via un wilcard, ce qui nous permet de couvrir le totalité des nom de domaines / sous-domaines et futurs sous-domaines sous la forme *.mon-domaine.fr


### Integration de Let's Encrypt dans une pipeline

Avec ce concept de wildcard, il est maintenant possible de gérer un seul certificat pour l'ensemble des domaines, ce qui automatise déjà la création d'un certificat par projet. 

Pour automatiser cette génération, j'ai intégré Let's Encrypt dans une pipeline Gitlab-ci, ce qui permet de nous passer d'un serveur pour le faire.

=> [Lien vers le job de la pipeline de génération](https://gitlab.com/gwendal-orinel/docker/blob/master/.gitlab-ci.yml#L84-107)

Cette Pipeline est appellée automatiquement tous les 1er du mois avec Gitscheldule (semblable à cron).


### Déploiement et livraison des certificats

Avec cette Pipeline Gitlab-Ci, une partie n'est pas couverte, c'est celle du chargement du certificat sur le serveur de l'application.

Deux cas sont étudiées:
 - Site hebergé avec Git-Pages, comme ce porte folio, les certificats doivent donc être poussés vers Gitlab directement
 - Site hebergé sur un serveur classique, il faudra donc pouvoir récupérer les fichiers directement et les faire portés par apache/nginix etc..
 

#### Cas Gitlab-Pages

Gitlab  Fournit une Api avec laquelle il est possible de pousser directement les certificats sur un domaine associé.

J'ai donc développé un script qui parcours la totalités de mes projets Gitlab, cherche si ils contiennent des Gitlab-Pages et si ils sont attachés à un nom de domaine.

Dans ce cas le certificat est récupéré de la pile de cache de la pipeline Let'Encrypt et est poussé directement par api sur le domaine du Gitlab-page.

=> [Lien vers le script](https://gitlab.com/gwendal-orinel/docker/blob/master/certbot/update-pages.sh)


#### Cas récuperation des livrables

Afin de pouvoir récupérer les certificats pour pouvoir les exploiter sur un autre serveur, j'ai utilisé les artifacts dans la pipeline Gitab-Ci.

La fonction artifacts permet d'archiver un contenu présent dans un job Gitlab-ci et de le proposer en téléchargement une fois la pipeline terminée.

Cette fonction peut être utile dans de nombreux cas, par exemple récuperer les sources/livrables d'une complation, récuperer des logs etc..

Il est donc possible de récuperer les certificats génénerés via l'interface Gitlab ou par API, c'est cette dernière qui nous intéresse.

Effectivement il suffit d'ajouter la commande suivante dans une tache cron mensuelle sur le serveur en question, et les certificats seront automatiquement actualisés


``` bash
 curl --output certificats.zip -L --header "PRIVATE-TOKEN: $TOKEN" https://gitlab.com/api/v4/projects/8641028/jobs/artifacts/master/download?job=certbot-renew
```


=> [Lien vers le job de la pipeline de déploiement](https://gitlab.com/gwendal-orinel/docker/blob/master/.gitlab-ci.yml#L109-126)
