---
title: Diplômes et Certifications
date: 2019-06-01  10:00:00
categories: About
index: false
toc: false
tags: 
 - CFI Montigny
---
<ul class="list-group">
{% for item in  site.data.diplomes %}
  <li class="list-group-item">
	<h4 id="{{ item.name }}">{{ item.name }}</h4>
	<span class="float-right badge badge-primary badge-pill" title="Duree de la formation">{% fa_inline history fas %} {{ item.duree }} ans</span>
	<p class="list-group-item-text">{{ item.desc }}</p>
	<a href="{{  site.data.ecoles[item.ecole].url }}"><h7> {% fa_inline graduation-cap fas %} {{  site.data.ecoles[item.ecole].name }} </h7></a>
	<footer class="blockquote-footer"><cite> Promotion {{ item.annee }}<cite></footer>
  </li>
{% endfor %}
</ul>

