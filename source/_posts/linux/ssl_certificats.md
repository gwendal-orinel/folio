---
title: Importation certificat SSL (ubuntu/centos)
date: 2019-08-02  10:00:00
tags:
 - shell
 - Https
categories: 
 - Linux
---

#### Importation certificat SSL (ubuntu/centos)

```
if [ "$(. /etc/os-release && echo "$ID")" = "ubuntu" ]; then
	CERT_DIR="/usr/local/share/ca-certificates"
	cp $APACHE_PATH/ssl/certs/cacert.pem $CERT_DIR/ca.crt
	---- OR ----
	openssl s_client -connect HOST:PORT -showcerts </dev/null 2>/dev/null | sed -e '/-----BEGIN/,/-----END/!d' | sudo tee -a $CERT_DIR/HOST.crt
	update-ca-certificates --fresh
else
	CERT_DIR="/etc/pki/ca-trust/source/anchors"
	cp $APACHE_PATH/ssl/certs/cacert.pem $CERT_DIR/ca.crt
	---- OR ----
	openssl s_client -connect HOST:PORT -showcerts </dev/null 2>/dev/null | sed -e '/-----BEGIN/,/-----END/!d' | sudo tee -a $CERT_DIR/HOST.crt
	update-ca-trust
fi	
```

