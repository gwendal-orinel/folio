---
title: Configuration réseau sous Ubuntu 18.04
date: 2019-08-02  12:00:00
tags:
 - Ubuntu
 - Dns
categories: 
 - Linux
---

Sur Ubuntu 18, la gestion du réseau est maintenant géré par Netplan au lieu de NetworkManager    
La gestion DNS peut être gérée par systemd ou intégrée à Netplan      


#### Gestion des DNS avec systemd


Editer le fichier resolved:
```
vi /etc/systemd/resolved.conf

[Resolve]
DNS=8.8.8.8 8.8.4.4
#FallbackDNS=
#Domains=
#LLMNR=no
#MulticastDNS=no
#DNSSEC=no
#Cache=yes
#DNSStubListener=yes
```

puis redemarer le service:
```
systemctl restart systemd-resolved.service
```

#### Gestion reseau avec netplan

Editer le fichier de configuration de netplan
```
vi /etc/netplan/50-cloud-init.yaml

```

Exemple de configuration:
```
network:
    version: 2
    ethernets:
        ens192:
            dhcp4: false
 	    addresses: [163.172.123.123/24, 212.83.123.123/32]
	    gateway4: 163.172.123.1
        ens33:
            dhcp4: true
            dhcp4-overrides:
              route-metric: 50
            nameservers:
              addresses: [8.8.8.8]
    wifis:
      wlp2s0b1:
        dhcp4: no
        dhcp6: no
        addresses: [192.168.0.21/24]
        gateway4: 192.168.0.1
        nameservers:
          addresses: [192.168.0.1, 8.8.8.8]
        access-points:
          "network_ssid_name":
            password: "**********"
        
```



puis appliquer les modifications:
```
sudo netplan apply
```

