---
title: Menu en Shell
date: 2019-08-02  10:00:00
tags:
 - shell
 - shell menu
categories: 
 - Linux
---

#### Create Usage function

```
USAGE(){
echo "-e <env>, --env=<value>	Précise l'environnement cible (--env=dev, --env=rec, --env=prod, etc)"
echo "-u, --uninstall		Désinstalle le Chatbot"
echo "-d, --delete-volumes	Supprime les données persistantes du Chatbot (peut être cumulé avec l'option --uninstall)" 
echo "-p, --pull		Télécharge seulement les images nécessaires au Chatbot"
echo "-h, --help		Affiche cette page d'aide"
echo 
echo "Si aucune option n'est précisée, le script lance un déploiement du Chatbot en mode interactif"
echo
}
```

#### Using Menu

```
OPTS=$(getopt --options "e:,h,u,d,p" --long "env:,help,uninstall,delete-volumes,pull" --name "$0" -- "$@")
[[ $? != 0 ]] && { echo "Echec à la lecture des options !" >&2 ; exit 1; }
eval set -- "$OPTS"

while true ; do
	case "$1" in
		-e | --env )  ENV="$2"; shift 2 ;;
		-p | --pull ) DOWNLOAD_IMAGES; EXIT_STATUS=true; shift ;;
		-u | --uninstall ) CLEAN_ENV; EXIT_STATUS=true; shift ;;
		-d | --delete-volumes ) CLEAN_PERSISTENT_DATA; shift ;;
		-h | --help ) USAGE; exit 1;;
		-- ) shift; break ;;
		* ) USAGE; exit 1;;
	esac
done
[[ $EXIT_STATUS ]] && exit 0;
```

