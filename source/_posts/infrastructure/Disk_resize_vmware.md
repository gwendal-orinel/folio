---
title: Redimensionnement d'un disque vmware suite à une augmentation de disque alloué 
date: 2019-06-27  15:00:00
tags:
 - vmware
 - redimensionnement disque
categories: 
 - Infrastructure
---

#### Redimensionnement d'un disque d'une machnie virtuelle sous vmware 

Après avoir augmenté l'espace alloué d'un disque il se peut que la vm ne voit pas son disque agrandit.

Il faut donc taper les commandes suivantes:

```
gdisk /dev/sda
	Command (? for help): w
	Warning! Secondary header is placed too early on the disk! Do you want to
	correct this problem? (Y/N): y
	Do you want to proceed? (Y/N): y
reboot now
fdisk /dev/sda
	Command (m for help): p
	Command (m for help): d
	Partition number (1,2, default 2): 2
	Command (m for help): n
	Partition number (2-128, default 2): 2
	First sector (4096-209715166, default 4096): 4096
	Last sector, +sectors or +size{K,M,G,T,P} (4096-209715166, default 209715166): 209715166
	Do you want to remove the signature? [Y]es/[N]o: y
	Command (m for help): w
reboot now
resize2fs /dev/sda2
```

